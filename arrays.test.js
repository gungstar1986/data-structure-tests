const {
  getUpperCaseLetters,
  getCapitalizeWords,
  getReversWords,
  calculateCart,
  fourOfKind,
} = require('../tasks/arrays');

test('getUpperCaseLetters', () => {
  expect(getUpperCaseLetters('afHDKeafafNDJW rF')).toBe('HDKNDJWF');
  expect(getUpperCaseLetters('asdfasdf43435r')).toBe('');
  expect(getUpperCaseLetters([])).toBe('');
  expect(getUpperCaseLetters(null)).toBe('');
  expect(getUpperCaseLetters({})).toBe('');
  expect(getUpperCaseLetters(true)).toBe('');
});

test('getCapitalizeWords', () => {
  expect(getCapitalizeWords('afHDKeafafNDJW Guda nag Bfasdf')).toEqual(['Guda', 'Bfasdf']);
  expect(getCapitalizeWords('asdfasdf43435r afd ac')).toEqual([]);
  expect(getCapitalizeWords([])).toEqual([]);
  expect(getCapitalizeWords(null)).toEqual([]);
  expect(getCapitalizeWords({})).toEqual([]);
  expect(getCapitalizeWords(true)).toEqual([]);
});

test('getReversWords', () => {
  expect(getReversWords('hello world !? good')).toBe('olleh dlrow ?! doog');
  expect(getReversWords([])).toBe('');
  expect(getReversWords(null)).toBe('');
  expect(getReversWords({})).toBe('');
  expect(getReversWords(true)).toBe('');
});

test('calculateCart', () => {
  expect(calculateCart({
    1: 1.14,
    2: 2.51
  })).toBe(3.65);
  expect(calculateCart({
    1: '1.14',
    2: '2.51'
  })).toBe(0);
  expect(calculateCart({
    1: '1.14',
    2: 2.51,
    3: 3.11
  })).toBe(5.62);
});

test('fourOfKind', () => {
  expect(fourOfKind([1, 2, 3, 4, 4, 4, 4, 5])).toBe(4);
  expect(fourOfKind([1, 2, 3, 4, 4, '4', 4, 5])).toBe(0);
  expect(fourOfKind([1, 1, 1, 1, 4, '4', 4, 5, 5, 5, 5])).toBe(5);
  expect(fourOfKind([])).toBe(0);
});